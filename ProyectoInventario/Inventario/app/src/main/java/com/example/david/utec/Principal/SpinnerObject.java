package com.example.david.utec.Principal;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class SpinnerObject extends Activity {

	private int databaseId;
	private String databaseValue;
	private String databaseValue1;
	private String databaseValue2;
	private String databaseValue3;
	

	public SpinnerObject(int databaseId, String databaseValue ) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
		
	}
	
	/*public SpinnerObject(int databaseId, String databaseValue, String databaseValue1 ) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
		this.databaseValue1 = databaseValue1;
		
	}*/
	
	public SpinnerObject(int databaseId , String databaseValue , String databaseValue1 ) {
		this.databaseId = databaseId;
		this.databaseValue = databaseValue;
		this.databaseValue1 = databaseValue1;
		//this.databaseValue3 = databaseValue3;
	}
	public int getId() {
		return databaseId;
	}

	
	
	public String getValue() {
		return databaseValue;
	}
	
	public String getValue1() {
		return databaseValue1;
	}
	
	public String getValue2() {
		return databaseValue2;
	}
	
	
	
	
	public String getValue3() {
		return databaseValue3;
	}

	@Override
	public String toString() {
		return databaseValue;
	}
	
	public String toString1() {
		return databaseValue1;
	}
}
