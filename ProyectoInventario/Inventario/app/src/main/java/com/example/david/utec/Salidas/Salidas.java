package com.example.david.utec.Salidas;

import android.app.Dialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.text.TextWatcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;

import com.example.david.utec.Detalle.Detalle;
import com.example.david.utec.Detalle.ModificarEliminarDetalle;
import com.example.david.utec.Detalle.ReporteDetalles;
import com.example.david.utec.Principal.*;

import com.example.david.utec.R;

import java.io.IOException;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Salidas extends Activity {


    public SQLiteDatabase conxOpen;
    public Context contexto= this;
    public Context context = this;
    public GestionDB objGestionDB;
    public  GestionDB obj;

    Dialog customDialog= null;

    String nombreusuario, id_proveedor, id_producto;
    Spinner sp_proveedor, sp_producto;

    int id_cliente=0;
    EditText textobuscar, cliente;
    String filtro="";
    ListView listaPaciente;

    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<ReporteSalidas> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_salidas);


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_salidas);

        Bundle bundle= getIntent().getExtras();
        nombreusuario= bundle.getString("var_user");

      //  Toast.makeText(this, "" + nombreusuario, Toast.LENGTH_SHORT).show();

        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        mTableLayoutReporte=(TableLayout)findViewById(R.id.tablelayout_salidas);
        CrearConsulta();

        sp_proveedor=(Spinner)findViewById(R.id.spinnerproveedorsalidas);
        sp_producto=(Spinner)findViewById(R.id.spinnerproductosalidas);

        cargarspinerproveedor();

        // SACAR ID DEL COMBO proveedor SELECCIONADO
        sp_proveedor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_proveedor = (((SpinnerObjectString) sp_proveedor.getSelectedItem()).getCodigo());
                cargarspinerproducto(id_proveedor);


            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });


        // sacar id producto
        sp_producto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_producto = (((SpinnerObjectString) sp_producto.getSelectedItem()).getCodigo());
                //loadSpinnerDataSp_municipio(id_depto);// llamo al metodo que va a cargar los municipios
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        cliente  = (EditText)findViewById(R.id.editTextclientesalidas);

        textobuscar = (EditText)findViewById(R.id.editTextclientesalidas);
        textobuscar.addTextChangedListener(new TextWatcher() {




            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (textobuscar.length() != 0) {
                    filtro = " LIKE '%" + textobuscar.getText().toString() + "%' ";
                    loadPaciente();
                }
            }
        });


        listaPaciente = (ListView) findViewById(R.id.ListViewcliente);
        loadPaciente();

        listaPaciente.setTextFilterEnabled(true);

        listaPaciente.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> elemento, View v, int position, long id) {
                id_cliente = ((SpinnerObject) elemento.getItemAtPosition(position)).getId();

               /* Intent i = new Intent(AgregarExaPaciente.this, Agregar.class);
                i.putExtra("id", elegido);
                startActivity(i); */
                setiar(id_cliente);

                //DialogFragment dialog = new GeneralDialog(R.drawable.ic_pregunta,R.string.Asigexapa,R.string.Asigexapa);
                //dialog.show(getFragmentManager(), "BaseDatosError");

            }
        });



    }


    public void CrearConsulta(){

        mListaActividades=objGestionDB.generartablasalidas(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (ReporteSalidas fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewcliente        = new TextView(context);
            TextView textViewproveedor      = new TextView(context);
            TextView textViewNombreproduto  = new TextView(context);
            TextView textViewcantidad       = new TextView(context);
            TextView textViewlote           = new TextView(context);
            TextView textViewvencimiento    = new TextView(context);
            TextView id                     = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewcliente.setText((fila.getcliente()));
            textViewproveedor.setText((fila.getproveedor()));
            textViewNombreproduto.setText((fila.getnombre()));
            textViewcantidad.setText((fila.getcantidad()));
            textViewlote.setText(fila.getlote());

            textViewvencimiento.setText(fila.getfechavencimiento());
            id.setText((fila.getid()));


            /*
               COLUMNA NOMBRE cliente
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewcliente.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewcliente.setGravity(Gravity.CENTER);
            textViewcliente.setTextSize(15);
            textViewcliente.setTypeface(null, Typeface.BOLD);
            textViewcliente.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreclienteTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreclienteTableRowParams.setMargins(1, 1, 1, 1);
            nombreclienteTableRowParams.width = 196;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewcliente, nombreclienteTableRowParams);

            // FIN COLUMNA NOMBRE cliente



            /*
               COLUMNA NOMBRE PROVEEDOR
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewproveedor.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewproveedor.setGravity(Gravity.CENTER);
            textViewproveedor.setTextSize(15);
            textViewproveedor.setTypeface(null, Typeface.BOLD);
            textViewproveedor.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproveedorTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproveedorTableRowParams.setMargins(1, 1, 1, 1);
            nombreproveedorTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewproveedor, nombreproveedorTableRowParams);

            // FIN COLUMNA NOMBRE





            /*
               COLUMNA NOMBRE PRODUCTO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreproduto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreproduto.setGravity(Gravity.CENTER);
            textViewNombreproduto.setTextSize(15);
            textViewNombreproduto.setTypeface(null, Typeface.BOLD);
            textViewNombreproduto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproductoTableRowParams.setMargins(1, 1, 1, 1);
            nombreproductoTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreproduto, nombreproductoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA CANTIDAD
                     */
            //Asignamos el color
            textViewcantidad.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewcantidad.setGravity(Gravity.CENTER);
            textViewcantidad.setTextSize(15);
            textViewcantidad.setTypeface(null, Typeface.BOLD);
            textViewcantidad.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams cantidadTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            cantidadTableRowParams.setMargins(1, 1, 1, 1);
            cantidadTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewcantidad, cantidadTableRowParams);

            //FIN COLUMNA CODIGO.

                /*
                COLUMNA LOTE
                 */
            textViewlote.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewlote.setGravity(Gravity.CENTER);
            textViewlote.setTextSize(15);
            textViewlote.setTypeface(null, Typeface.BOLD);
            textViewlote.setHeight(50);

            TableRow.LayoutParams loteTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            loteTableRowParams.setMargins(1, 1, 1, 1);
            loteTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewlote, loteTableRowParams);

            // FIN COLUMNA LOTE









            /*
            columna  FECHA VENCIMIENTO.
             */
            textViewvencimiento.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewvencimiento.setGravity(Gravity.CENTER);
            textViewvencimiento.setTextSize(15);
            textViewvencimiento.setTypeface(null, Typeface.BOLD);
            textViewvencimiento.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams vencimientoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            vencimientoTableRowParams.setMargins(1, 1, 1, 1);
            vencimientoTableRowParams.width = 130;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewvencimiento, vencimientoTableRowParams);
            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));









            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {


                    tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));


                  /*  Intent i = new Intent(Salidas.this, ModificarEliminarDetalle.class);
                    i.putExtra("s_id",s_id);
                    i.putExtra("var_user" ,nombreusuario);
                    i.putExtra("id_proveedor" ,id_proveedor);
                    i.putExtra("id_producto" ,id_producto);
                    i.putExtra("id_cliente" ,id_cliente);




                    startActivity(i); */





                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }


    public void  setiar(int id_cliente){

        objGestionDB.cliente(contexto, id_cliente);

        cliente  = (EditText)findViewById(R.id.editTextclientesalidas);
        cliente.setText(objGestionDB.nombre_cliente);

       // Toast.makeText(this,"es"+id_cliente, Toast.LENGTH_SHORT).show();

    }

    private void loadPaciente()

    {
        List<SpinnerObject> lista =  objGestionDB.listaPacientes(contexto, filtro);
        ArrayAdapter<SpinnerObject> adap = new ArrayAdapter<SpinnerObject>(Salidas.this, R.layout.simplerow, lista);
        adap.setDropDownViewResource(R.layout.simplerow);
        listaPaciente.setAdapter(adap);
    }




    public void cargarspinerproveedor(){
        // CARGAR COMBO PROVEEDOR --> SPINNER

        List<SpinnerObjectString> lables = objGestionDB.getProveedor(this.contexto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_proveedor.setAdapter(dataAdapter);



    }

    public void cargarspinerproducto(String id_proveedor){
        // CARGAR COMBO PRODUCTO --> SPINNER

        //   Toast.makeText(this,""+id_proveedor,Toast.LENGTH_SHORT).show();

        List<SpinnerObjectString> lables = objGestionDB.getProducto(this.contexto, id_proveedor);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_producto.setAdapter(dataAdapter);



    }

    public  void guardarsalida( View guardarsalida){

        Toast.makeText(this,"es"+id_cliente, Toast.LENGTH_SHORT).show();
    }

   // BUSCAR
    public  void  click_buscarsalidas(View buscar){


        String s_cliente = cliente.getText().toString().trim();



        String idcliente = "";



        idcliente = String.valueOf(id_cliente);

        idcliente= Integer.toString(id_cliente);



        //  Toast.makeText(this,idcliente ,Toast.LENGTH_SHORT).show();

        int respuesta=  validarbusqueda(id_proveedor, id_producto, id_cliente, s_cliente);


        if (respuesta==0){

           // Toast.makeText(this, "bien buscar",Toast.LENGTH_SHORT).show();

            mTableLayoutReporte.removeAllViews();
            mListaActividades=objGestionDB.generartablasalidasbusqueda(contexto, id_proveedor, id_producto, id_cliente);
            crearTabla();

        }else {

              informar();
        }



    }

    public void menusalidas ( View menu){

        Intent i= new Intent(this, menu.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);


    }


    public void  onclick_detalle (View detalle){

        String s_cliente = cliente.getText().toString().trim();



        String idcliente = "";



        idcliente = String.valueOf(id_cliente);

        idcliente= Integer.toString(id_cliente);



      //  Toast.makeText(this,idcliente ,Toast.LENGTH_SHORT).show();

      int respuesta=  validar(id_proveedor, id_producto, id_cliente, s_cliente);

        if(respuesta==0){

            objGestionDB.insertarSalida(contexto, idcliente);  // insertamos la salida del cliente

            Intent i= new Intent(this, Detalle.class);
            i.putExtra("var_user",nombreusuario);
            i.putExtra("id_proveedor",id_proveedor);
            i.putExtra("id_producto",id_producto);
            i.putExtra("idcliente",idcliente);
            finish();
            startActivity(i);


        }
        else {
            errorcompletarcampo();
        }
    }


    public  int validar(String id_proveedor, String id_producto, int id_cliente, String s_cliente){
        int respuesta =0;

        if( (id_cliente==0)  ||  (s_cliente.equals(""))   ){

            respuesta=1;

        }

        if(id_proveedor.equals("0")){
            respuesta=1;
        }

        if(id_producto.equals("0")){
            respuesta=1;
        }

        return respuesta;
    }

    public  int validarbusqueda(String id_proveedor, String id_producto, int id_cliente, String s_cliente){
        int respuesta =0;

        if( (id_cliente==0) &&  (s_cliente.equals(""))  &&   (id_proveedor.equals("0"))  &&  (id_producto.equals("0"))  ){

            respuesta=1;

        }



        return respuesta;
    }


    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }

    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Debe de Ingresar al menos un Parámetro, Para Realizar la Búsqueda..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }
}
