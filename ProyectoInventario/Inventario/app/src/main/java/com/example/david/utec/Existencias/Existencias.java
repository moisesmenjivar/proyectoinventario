package com.example.david.utec.Existencias;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Principal.SpinnerObjectString;
import com.example.david.utec.R;
import com.example.david.utec.Principal.*;
import com.example.david.utec.Salidas.ReporteSalidas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Existencias extends Activity {

    public SQLiteDatabase conxOpen;
    public Context contexto= this;
    public Context context = this;
    public GestionDB objGestionDB;
    public  GestionDB obj;

    Dialog customDialog= null;

    String nombreusuario, id_proveedor, id_producto;
    Spinner sp_proveedor, sp_producto;


    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<ReporteExistencias> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_existencias);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_existencias);




        Bundle bundle= getIntent().getExtras();
        nombreusuario= bundle.getString("var_user");


        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }




        mTableLayoutReporte=(TableLayout)findViewById(R.id.tablelayout_existencias);
        CrearConsulta();

        sp_proveedor=(Spinner)findViewById(R.id.spinnerproveedorsalidas);
        sp_producto=(Spinner)findViewById(R.id.spinnerproductosalidas);

        cargarspinerproveedor();

        // SACAR ID DEL COMBO proveedor SELECCIONADO
        sp_proveedor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_proveedor = (((SpinnerObjectString) sp_proveedor.getSelectedItem()).getCodigo());
                cargarspinerproducto(id_proveedor);


            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });


        // sacar id producto
        sp_producto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_producto = (((SpinnerObjectString) sp_producto.getSelectedItem()).getCodigo());
                //loadSpinnerDataSp_municipio(id_depto);// llamo al metodo que va a cargar los municipios
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


    }

    public void CrearConsulta(){

        mListaActividades=objGestionDB.generartablasExistencias(contexto);
       // mListaActividades=objGestionDB.generartablasalidas(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (ReporteExistencias fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewProveedor        = new TextView(context);
            TextView textViewNombreproduto  = new TextView(context);
            TextView textViewcantidad       = new TextView(context);
            TextView textViewlote           = new TextView(context);
            TextView textViewvencimiento    = new TextView(context);
           // TextView id                     = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewProveedor.setText((fila.getproveedor()));
            textViewNombreproduto.setText((fila.getnombre()));
            textViewcantidad.setText((fila.getcantidad()));
            textViewlote.setText(fila.getlote());

            textViewvencimiento.setText(fila.getfechavencimiento());
           // id.setText((fila.getid()));


            /*
               COLUMNAproveedor
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewProveedor.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewProveedor.setGravity(Gravity.CENTER);
            textViewProveedor.setTextSize(15);
            textViewProveedor.setTypeface(null, Typeface.BOLD);
            textViewProveedor.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams proveedorTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            proveedorTableRowParams.setMargins(1, 1, 1, 1);
            proveedorTableRowParams.width = 197;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewProveedor, proveedorTableRowParams);

            // FIN COLUMNA NOMBRE proveedor




            /*
               COLUMNA NOMBRE PRODUCTO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreproduto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreproduto.setGravity(Gravity.CENTER);
            textViewNombreproduto.setTextSize(15);
            textViewNombreproduto.setTypeface(null, Typeface.BOLD);
            textViewNombreproduto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproductoTableRowParams.setMargins(1, 1, 1, 1);
            nombreproductoTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreproduto, nombreproductoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA CANTIDAD
                     */
            //Asignamos el color
            textViewcantidad.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewcantidad.setGravity(Gravity.CENTER);
            textViewcantidad.setTextSize(15);
            textViewcantidad.setTypeface(null, Typeface.BOLD);
            textViewcantidad.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams cantidadTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            cantidadTableRowParams.setMargins(1, 1, 1, 1);
            cantidadTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewcantidad, cantidadTableRowParams);

            //FIN COLUMNA CODIGO.

                /*
                COLUMNA LOTE
                 */
            textViewlote.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewlote.setGravity(Gravity.CENTER);
            textViewlote.setTextSize(15);
            textViewlote.setTypeface(null, Typeface.BOLD);
            textViewlote.setHeight(50);

            TableRow.LayoutParams loteTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            loteTableRowParams.setMargins(1, 1, 1, 1);
            loteTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewlote, loteTableRowParams);

            // FIN COLUMNA LOTE



            /*
            columna  FECHA VENCIMIENTO.
             */
            textViewvencimiento.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewvencimiento.setGravity(Gravity.CENTER);
            textViewvencimiento.setTextSize(15);
            textViewvencimiento.setTypeface(null, Typeface.BOLD);
            textViewvencimiento.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams vencimientoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            vencimientoTableRowParams.setMargins(1, 1, 1, 1);
            vencimientoTableRowParams.width = 130;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewvencimiento, vencimientoTableRowParams);
            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));










            mTableLayoutReporte.addView(tableRow);

        }

    }



    public  void click_buscarexistencia (View buscar){

       // Toast.makeText(this,""+id_proveedor+id_producto , Toast.LENGTH_SHORT).show();

      int respuesta=  validar(id_proveedor,id_producto);

        if (respuesta==0){

            mTableLayoutReporte.removeAllViews();
            mListaActividades=objGestionDB.generartablasExistenciasbusqueda(contexto, id_proveedor,  id_producto);
            crearTabla();
        }else {

            informar();
        }



    }

    public  void click_menuexistencias (View menuregresas){


        Intent i = new Intent(this, menu.class);
        i.putExtra("var_user", nombreusuario);
        finish();
        startActivity(i);

    }

    public int validar(String id_proveedor, String id_producto){
        int respuesta=0;

        if (  (id_proveedor=="0")  &&  (id_producto=="0") ){

            respuesta=1;

        }

        return respuesta;

    }


    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Debe de Ingresar al menos un Parámetro, Para Realizar la Búsqueda..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void cargarspinerproveedor(){
        // CARGAR COMBO PROVEEDOR --> SPINNER

        List<SpinnerObjectString> lables = objGestionDB.getProveedor(this.contexto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_proveedor.setAdapter(dataAdapter);



    }


    public void cargarspinerproducto(String id_proveedor){
        // CARGAR COMBO PRODUCTO --> SPINNER

        //   Toast.makeText(this,""+id_proveedor,Toast.LENGTH_SHORT).show();

        List<SpinnerObjectString> lables = objGestionDB.getProducto(this.contexto, id_proveedor);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_producto.setAdapter(dataAdapter);



    }

}
