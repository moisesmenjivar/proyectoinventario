/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.example.david.utec.Entradas;

import java.util.ArrayList;

public class TablaEntradas {
	public String proveedor;						                        //Esta cadena representa la fecha en que debe de realizarse las actividades
	public String producto;
	public String cantidad_producto;
	public String fechavencimiento;
    public String fecha;
	public String estado;
	public String id;
	public ArrayList<String> Actividades = new ArrayList<String>();		//Esta es la lista de todas las actividades que se realizan es la misma fecha


	// proveedor

	public String getproveedor() {
		return proveedor;
	}
	public void setproveedor(String proveedor) {
		proveedor = proveedor;
	}


		// producto
	public String getproducto() {
		return producto;
	}
	public void setproducto(String producto) {
		producto = producto;
	}


	// cantidad_producto
	public String getcantidad_producto() {
		return cantidad_producto;
	}
	public void setcantidad_producto(String cantidad_producto) {
		cantidad_producto = cantidad_producto;
	}

		// fecha
	public String getfecha() {
		return fecha;
	}
	public void setfecha(String fecha) {
		fecha = fecha;
	}

	//  fechavencimiento
	public String getfechavencimiento() {
		return fechavencimiento;
	}
	public void setfechavencimiento(String fechavencimiento) {
		fechavencimiento = fechavencimiento;
	}


	// estado
	public String getestado() {
		return estado;
	}
	public void setestado(String estado) {
		estado = estado;
	}


	// id_entrada
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	


	
	

}
