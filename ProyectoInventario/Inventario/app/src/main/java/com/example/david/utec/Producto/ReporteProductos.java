/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.example.david.utec.Producto;

import java.util.ArrayList;

public class ReporteProductos {
	public String nombre_producto;						                        //Esta cadena representa la fecha en que debe de realizarse las actividades
	public String codigo_producto;
	public String proveedor;
	public String estado;
	public String id;
	public ArrayList<String> Actividades = new ArrayList<String>();		//Esta es la lista de todas las actividades que se realizan es la misma fecha


	// NOMBRE del producto

	public String getnombre() {
		return nombre_producto;
	}
	public void setnombre(String nombre_producto) {
		nombre_producto = nombre_producto;
	}


		// codigo del producto
	public String getcodigo_producto() {
		return codigo_producto;
	}
	public void setcodigo_producto(String codigo_producto) {
		codigo_producto = codigo_producto;
	}


	// ESTADO
	public String getestado() {
		return estado;
	}
	public void setestado(String estado) {
		estado = estado;
	}

		// proveedor
	public String getproveedor() {
		return proveedor;
	}
	public void setproveedor(String proveedor) {
		proveedor = proveedor;
	}





	// id producto
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	


	

}
