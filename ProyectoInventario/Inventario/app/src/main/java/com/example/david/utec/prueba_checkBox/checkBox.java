package com.example.david.utec.prueba_checkBox;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Principal.*;
import com.example.david.utec.R;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class checkBox extends Activity {


   // public SQLiteDatabase conexOpen;
    public Context contexto = this;
    public GestionDB objGestionDB;



    LinearLayout linearMain;
    CheckBox checkBox;
    EditText nombre_canto, letra_canto;
    TextView titulo;
    Button botonaccion;


    //----------------------------------------------------------------------------------------------
    public SQLiteDatabase conexOpen;// contiene la conexion a la base de datos
    private Context ccontexto;


    public void conectarDB(Context contexto) {
        BaseDeDatos objBaseDeDatos = new BaseDeDatos(ccontexto);
        objBaseDeDatos.openDataBase();
        this.conexOpen = objBaseDeDatos.myDataBase;// guardo la conexion a la base de datos

    }
    public void desConectarDB() {
        if (this.conexOpen != null){
            this.conexOpen.close();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);



        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }




       // titulo       = (TextView) findViewById(R.id.textViewbienvenidoscantos);
       // nombre_canto = (EditText) findViewById(R.id.editTextcanto1);
       // letra_canto  = (EditText) findViewById(R.id.editTextcanto2);
        botonaccion  = (Button) findViewById(R.id.buttonaccioncanto);
        linearMain   = (LinearLayout) findViewById(R.id.linearMain);



        conectarDB(contexto);
        LinkedHashMap<String, String> names = new LinkedHashMap<String, String>();
        Cursor cursor = conexOpen.rawQuery("SELECT id, alias  FROM proveedor order by alias ASC",null);

        if (cursor.moveToFirst())
        {
            do {
                names.put(String.valueOf(cursor.getInt(0)), cursor.getString(1));
            } while (cursor.moveToNext());


        }
        desConectarDB();

        Set<?> set = names.entrySet();
        // Get an iterator
        Iterator<?> i = set.iterator();

        while (i.hasNext())
        {
            @SuppressWarnings("rawtypes")
            Map.Entry me = (Map.Entry) i.next();
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());

            checkBox = new CheckBox(this);
            checkBox.setId(Integer.parseInt(me.getKey().toString()));
            checkBox.setText(me.getValue().toString());
          //  checkBox.setOnClickListener(getOnClickDoSomething(checkBox));
            linearMain.addView(checkBox);
        }

    }


    public  void click_aceptar_prueba (View prueba){

        int  bandera_seleccionado=0;
        conectarDB(contexto);
        Cursor cursor = conexOpen.rawQuery("SELECT id, alias  FROM proveedor order by alias ASC",null);

        if (cursor.moveToFirst())
        {
            do
            {   int identificador     = cursor.getInt(0);
                CheckBox cajadecheque = (CheckBox) findViewById(identificador);
                if(cajadecheque.isChecked()==true)
                {
                    bandera_seleccionado=bandera_seleccionado+1;
                }
            } while (cursor.moveToNext());
        }desConectarDB();


        if(bandera_seleccionado==0)
        {
            Toast toastusuarioyaexiste = Toast.makeText(getApplicationContext(), "No ha seleccionado ningun Proveedor", Toast.LENGTH_SHORT);
            toastusuarioyaexiste.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
            toastusuarioyaexiste.setGravity(Gravity.BOTTOM, 0, 0);
            toastusuarioyaexiste.show();

        }else
        {   conectarDB(contexto);
            Cursor cursor2 = conexOpen.rawQuery("SELECT id, alias  FROM proveedor order by alias ASC",null);

            if (cursor2.moveToFirst())
            {
                do
                {
                    int identificador2     = cursor2.getInt(0);
                    CheckBox cajadecheque2 = (CheckBox) findViewById(identificador2);
                    String proveedor       = Integer.toString(identificador2);

                    if(cajadecheque2.isChecked()==true)
                    {
                        objGestionDB.insertProducto(contexto,"s_nombre_producto","s_codigo_producto", proveedor,"1");
                    }
                } while (cursor2.moveToNext());
            }desConectarDB();
            Toast toast = Toast.makeText(this, "EL Producto se ha Registrado", Toast.LENGTH_LONG);
            toast.show();


           // Intent i = new Intent(this, menu.class);
           // i.putExtra("var_user","moy");
           // finish();
           // startActivity(i);
        }
    }


    public  void cancelarprueba (View cancelarPrueba){
        Intent i = new Intent(this, menu.class);
        i.putExtra("var_user","moy");
        finish();
        startActivity(i);
    }



}
