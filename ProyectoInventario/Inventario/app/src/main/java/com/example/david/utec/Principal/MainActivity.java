package com.example.david.utec.Principal;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;
import android.os.Bundle;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.WindowManager;


import com.example.david.utec.R;

import java.io.IOException;

public class MainActivity extends FragmentActivity {

    Dialog customDialog = null;


    private EditText username, password;



    public SQLiteDatabase conexOpen;
    public Context contexto = this;

    public GestionDB objGestionDB; // = new GestionDB(this);//creo el objeto de
                                   // la clase que gestiona la DB





    @Override
    /*protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_main);


        username=(EditText) findViewById(R.id.editTextusuario); // TOMO EL LO QUE CONTIENE EL EDITTEXT USUARIO
        password=(EditText) findViewById(R.id.editpass); // TOMO EL LO QUE CONTIENE EL EDITTEXT pass

        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }




    }




    /*
    BOTON ACEPTAR
     */


    public void click_aceptar(View aceptar){


        String s_username = username.getText().toString().trim();
        String s_password = password.getText().toString().trim();

        int respuesta = 0;
        if (s_username.equals("")){
            //Toast.makeText(this, "Digite el usuario ", Toast.LENGTH_LONG).show();
            errorusuario();
        } else if (s_password.equals("")){
            //Toast.makeText(this, "Digite el password", Toast.LENGTH_LONG).show();
            errorusuario();
        }

        //Toast.makeText(this, s_username+s_password, Toast.LENGTH_LONG).show();

        else {
            respuesta = objGestionDB.consultarUser(s_username, s_password,contexto);// llamo el  metodo que consulta  a la db
            if (respuesta == 0)
            {
                Toast.makeText(this, "El usuario no existe", Toast.LENGTH_LONG).show();
            }
            else if (respuesta == 2)
            {
                Toast.makeText(this, "La password es incorrecta",Toast.LENGTH_LONG).show();
            }
            else if (respuesta == 1)
            {
                //Toast.makeText(	this,"todo bien ",Toast.LENGTH_LONG).show();
                //Login

                //Todo esta bien llamo el metodo que guarda las variables en shared preferences
                //sesiones_SP();
                //Toast.makeText(this,"Todo bien lazar la Acttivity que tiene el menu", Toast.LENGTH_LONG).show();

                Intent i = new Intent(this, menu.class);
                i.putExtra("var_user",s_username);
                finish();
                startActivity(i);
            }
            else {
                Toast.makeText(	this,"Ha ocurrido un error no se puedo consultar la informacion",Toast.LENGTH_LONG).show();
            }
        }

    }

    public void errorusuario()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });

	    /*((Button) customDialog.findViewById(R.id.cancelar)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view)
			{
				customDialog.dismiss();
				//Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

			}
		});*/

        customDialog.show();
    }




   /* @Override
    public boolean onCreateOptionsMenu(Menusecundario menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

  /*  protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }*/

  /*  public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }*/

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

}
