package com.example.david.utec.Clave;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.R;
import com.example.david.utec.Principal.*;

import java.io.IOException;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class cambio_de_clave extends Activity {

    Dialog customDialog = null;


    private EditText  pasword,  passActual, nueva_password, confirma_password;
    String nombreusuario;


    public SQLiteDatabase conexOpen;
    public Context contexto = this;

    public GestionDB objGestionDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_cambio_de_clave);




        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_cambio_de_clave);


        Bundle bundle 	= getIntent().getExtras();
        nombreusuario   = bundle.getString("var_user");

         Toast.makeText(this,"-->"+nombreusuario, Toast.LENGTH_LONG).show();

        passActual=(EditText) findViewById(R.id.editTexrpassActual);
        nueva_password=(EditText) findViewById(R.id.editTextnuevopasword);
        confirma_password=(EditText) findViewById(R.id.editpassconfirmar);


        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }




    public void click_guardar_pass(View guardarPass){

        String s_passActual = passActual.getText().toString().trim();
        String s_nueva_password = nueva_password.getText().toString().trim();
        String s_confirma_password = confirma_password.getText().toString().trim();


     int respuesta=   validar (s_passActual, s_nueva_password, s_confirma_password);

        if (respuesta==0){

           // Toast.makeText(this,"bien", Toast.LENGTH_LONG).show();


         int respuestapas=objGestionDB.passActual(contexto, nombreusuario, s_passActual);

         if (respuestapas==1){

             if (s_nueva_password.equals(s_confirma_password)){



                int id_usuario= objGestionDB.idusario( contexto, nombreusuario);

                 objGestionDB.actualizarPassword(contexto, s_confirma_password, id_usuario);

                // Toast.makeText(this,"ambas iguales"+id_usuario, Toast.LENGTH_LONG).show();
                 confirmacion();

             }else {

               //  Toast.makeText(this,"la confirmacion de la contraseña no es igual", Toast.LENGTH_LONG).show();
                 String mensaje="la confirmacion de la contraseña no es igual";
                 informar(mensaje);
             }

         }else{

             //Toast.makeText(this,"contraseña actual no coninciden ", Toast.LENGTH_LONG).show();

             String mensaje="contraseña actual no coinciden ";
             informar(mensaje);
         }





        }else {
           // Toast.makeText(this,"complete", Toast.LENGTH_LONG).show();

            errorcompletarcampo();


        }

    }

    public  int validar ( String s_passActual,  String s_nueva_password,  String s_confirma_password){
        int respuesta =0;

        if ( (s_passActual.equals(""))  ||  (s_nueva_password.equals("")) ||  (s_confirma_password.equals(""))  ){

            respuesta=1;
        }
        return respuesta;
    }


    public  void click_retornar_menu_pass(View retornar){

        Intent  i= new Intent(this, menu.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);

    }


    public void informar(String mensaje)
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);

        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void  confirmacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Password Actualizada  Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i = new Intent(cambio_de_clave.this,menu.class);
                i.putExtra("var_user", nombreusuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();

    }

    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });


        customDialog.show();
    }
}
