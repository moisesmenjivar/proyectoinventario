package com.example.david.utec.Proveedor;

import android.app.Dialog;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.R;

import android.app.Dialog;
import android.media.Image;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.david.utec.Principal.MainActivity;
import com.example.david.utec.Principal.SpinnerObject;
import com.example.david.utec.R;
import com.example.david.utec.Principal.menu;

import com.example.david.utec.Principal.SpinnerObjectString;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;


import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import java.io.IOException;
import java.util.List;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.LinearLayout;
import android.widget.ArrayAdapter;
import java.util.ArrayList;
import android.view.Gravity;
import android.graphics.Typeface;
import android.view.ViewGroup.LayoutParams;

public class modificarElimiarProveedor extends FragmentActivity {

    public SQLiteDatabase conexOpen;
    public Context context = this;
    public GestionDB objGestionDB ;
    public GestionDB obj;

    String id_proveedor,nombreusuario;
    Dialog customDialog = null;

    public Context contexto = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.modificar_elimar);


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
       // this.setContentView(R.layout.modificar_elimar);

        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Bundle bundle = getIntent().getExtras();
        id_proveedor= bundle.getString("s_id");
        nombreusuario   = bundle.getString("var_user");

       // Toast.makeText(this, "--->"+id_proveedor+nombreusuario,Toast.LENGTH_LONG).show();
       // modificarEliminar(id_proveedor);

        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.modificar_elimar);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloModificarEliminar);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeModificarElimar);
        contenido.setText("¿Modificar o Eliminar?");



        ((ImageView) customDialog.findViewById(R.id.ima_cerrar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

                Intent i = new Intent(modificarElimiarProveedor.this, proveedor.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });
        // EDITAR
        ((ImageView) customDialog.findViewById(R.id.img_editar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //  customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(modificarElimiarProveedor.this, Modificar_Proveedor.class);
                i.putExtra("s_id",id_proveedor);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

                //  Toast.makeText(this, "id-->"+s_id, Toast.LENGTH_LONG).show();
            }
        });
        //ELIMINAR
        ((ImageView) customDialog.findViewById(R.id.img_eliminar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                 customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

                eliminar( id_proveedor);
            }
        });

        customDialog.show();


    }


    public void eliminar(final String id_proveedor){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Eliminar Proveedor");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Una vez que elimines al proveedor, no podras deshacer la acción");

        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
               // customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(modificarElimiarProveedor.this,proveedor.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });

        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
               // customDialog.dismiss();




                objGestionDB.eliminarProveedor(contexto, id_proveedor);
                confirmarEliminacion();



            }
        });



        customDialog.show();

    }


        public void  confirmarEliminacion(){

            // con este tema personalizado evitamos los bordes por defecto
            customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
            //deshabilitamos el título por defecto
            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //obligamos al usuario a pulsar los botones para cerrarlo
            customDialog.setCancelable(false);
            //establecemos el contenido de nuestro dialog
            customDialog.setContentView(R.layout.confirmacion);

            TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
            titulo.setText("");

            TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
            contenido.setText("Proveedor Eliminado Correctamente");

            ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                    //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                    // i.putExtra("var_user",nombreusuario);


                    Intent i= new Intent(modificarElimiarProveedor.this,proveedor.class);
                    i.putExtra("var_user",nombreusuario);
                    finish();
                    startActivity(i);

                }
            });



            customDialog.show();

        }

    public void modificarEliminar(String id_proveedor)
    {

        // String id_proveedor = s_id;


        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.modificar_elimar);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloModificarEliminar);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeModificarElimar);
        contenido.setText("¿Modificar o Eliminar?");



        ((ImageView) customDialog.findViewById(R.id.ima_cerrar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

            }
        });
        // EDITAR
        ((ImageView) customDialog.findViewById(R.id.img_editar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //  customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
               /* Intent i = new Intent(proveedor.this, modificarElimiarProveedor.class);
                i.putExtra("s_id",id_proveedor);
                startActivity(i);*/

                //  Toast.makeText(this, "id-->"+s_id, Toast.LENGTH_LONG).show();
            }
        });
        //ELIMINAR
        ((ImageView) customDialog.findViewById(R.id.img_eliminar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });

        customDialog.show();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modificar_elimiar_proveedor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
