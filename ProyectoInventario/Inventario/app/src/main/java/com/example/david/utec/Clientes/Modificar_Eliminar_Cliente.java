package com.example.david.utec.Clientes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.david.utec.Producto.Modificar_Producto;
import com.example.david.utec.Producto.Producto;
import com.example.david.utec.R;

import java.io.IOException;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Modificar_Eliminar_Cliente extends FragmentActivity {


    public SQLiteDatabase conxOpen;
    public GestionDB objGestionDB;
    public GestionDB obj;
    String  nombreusuario, id_cliente;

    Dialog customDialog = null;

    public Context contexto = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_modificar__eliminar__cliente);


        Bundle bundle= getIntent().getExtras();
        id_cliente=bundle.getString("s_id");
        nombreusuario=bundle.getString("var_user");

        // Toast.makeText(this,"-->"+id_producto+nombreusuario, Toast.LENGTH_SHORT).show();


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        // this.setContentView(R.layout.modificar_elimar);

        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

       /* Bundle bundle = getIntent().getExtras();
        id_proveedor= bundle.getString("s_id");
        nombreusuario   = bundle.getString("var_user");*/

        // Toast.makeText(this, "--->"+id_proveedor+nombreusuario,Toast.LENGTH_LONG).show();
        // modificarEliminar(id_proveedor);

        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.modificar_elimar);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloModificarEliminar);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeModificarElimar);
        contenido.setText("¿Modificar o Eliminar?");



        ((ImageView) customDialog.findViewById(R.id.ima_cerrar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

                Intent i = new Intent(Modificar_Eliminar_Cliente.this, Clientes.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });
        // EDITAR
        ((ImageView) customDialog.findViewById(R.id.img_editar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //  customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Modificar_Eliminar_Cliente.this, Modificar_Clientes.class);
                i.putExtra("s_id",id_cliente);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

                //  Toast.makeText(this, "id-->"+s_id, Toast.LENGTH_LONG).show();
            }
        });
        //ELIMINAR
        ((ImageView) customDialog.findViewById(R.id.img_eliminar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

                eliminar( id_cliente);

                //  Toast.makeText(this, "eliminar", Toast.LENGTH_LONG).show();
            }
        });

        customDialog.show();







    }



    public void eliminar(final String id_producto){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Eliminar Cliente");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Una vez que elimines al Cliente, no podras deshacer la acción");

        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {



                Intent i= new Intent(Modificar_Eliminar_Cliente.this,Clientes.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });

        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // customDialog.dismiss();




                objGestionDB.eliminarCliente(contexto, id_producto);
                confirmarEliminacion();



            }
        });



        customDialog.show();

    }

    public void  confirmarEliminacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Cliente Eliminado Correctamente..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Modificar_Eliminar_Cliente.this,Clientes.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modificar__eliminar__cliente, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
